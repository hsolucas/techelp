'use strict';

var app = {
		isLoading: true,
		template: document.querySelector('.chamadoTemplate'),
		loader: document.querySelector('.loader'),
		container: document.querySelector('.lista-chamados'),
		dialog: document.querySelector('.novo-chamado'),
		dialogDetalhe: document.querySelector('.detalhes-chamado'),
		listaChamados: [],
		etapaChamado: 1
	};

	app.toggleDlgAbreChamado = function(visible) {
		app.setLoading(false);
		if (visible) {
		  app.dialog.classList.add('dialog-container--visible');
		} else {
		  app.dialog.classList.remove('dialog-container--visible');
		}
	};

	app.toggleDlgDetalhesChamado = function(visible) {
		app.setLoading(false);
		if (visible) {
		  app.dialogDetalhe.classList.add('dialog-container--visible');
		} else {
		  app.dialogDetalhe.classList.remove('dialog-container--visible');
		}
	};

	app.toggleInfChamado = function(infVisible){
		var inf = document.querySelector('.principal-chamado');
		var res = document.querySelector('.resolucao-chamado');
		if(infVisible){
			res.setAttribute('hidden', true);
			inf.removeAttribute('hidden');
		}else{
			res.removeAttribute('hidden');
			inf.setAttribute('hidden', true);
		}
	}

	app.getSelEtapaChamado = function(btnClicked){
		var ref = 'etapa-';
		var etapa = 0;
		var index = -1;
		try{
			var classes = btnClicked.parentNode.parentNode.classList;
			classes.forEach(function(c){
				index = c.search(ref);
				if(index > -1){
					etapa = parseInt(c.slice(index + ref.length, c.length));
				}
			});
		}catch(ex){
			console.log(ex);
		}
		return etapa;
	}

	app.changeEtapaChamado = function(numeroEtapa){
		var confirmaChamado = document.querySelector('#btnConfirma');
		var totalEtapas = document.querySelector(".novo-chamado .dialog-body").childElementCount;

		app.etapaChamado = numeroEtapa;
		confirmaChamado.setAttribute('hidden', true);

		for(var i=1;i<=totalEtapas;i++){
			var etapa = document.querySelector('.etapa-' + i);
			if(i == numeroEtapa){
				etapa.removeAttribute("hidden");
				if(numeroEtapa == totalEtapas){
					confirmaChamado.removeAttribute("hidden");
				}
			}else{
				etapa.setAttribute("hidden", true);
			}
		}
	}

	app.atualizarChamados = function(){
		app.loader.removeAttribute('hidden');
		app.container.setAttribute('hidden', true);
		app.setLoading(true);
		app.atualizarListaUsuario();
	}

	app.mostrarListaChamados = function(){
		app.limparListaChamados();
		try{
			app.listaChamados.forEach(function(chamado){
				var linha = app.template.cloneNode(true);
				var idLinha = chamado.id;
	      		linha.classList.remove('chamadoTemplate');
				linha.querySelector('.categoria').textContent = chamado.categoria.descricao;
				linha.querySelector('.dataAbertura').textContent = chamado.dataAbertura;
				linha.querySelector('.status').textContent = chamado.status.descricao;
				linha.setAttribute('id', idLinha);
				linha.removeAttribute('hidden');
				app.container.appendChild(linha);
				document.getElementById(idLinha).addEventListener('click', function() {
					app.getDetalhesChamado(this.id);
					app.toggleDlgDetalhesChamado(true);
				});
			});
			if(app.isLoading){
				app.setLoading(false);
				app.container.removeAttribute('hidden');
			}
		}catch(ex){
			console.log(ex);
			app.limparListaChamados();
		}
	}

	app.limparListaChamados = function(){
		while (app.container.hasChildNodes()) {
			app.container.removeChild(app.container.firstChild);
		}
	}

	app.setLoading = function(isLoading){
		app.isLoading = isLoading;
		if(app.isLoading){
			app.loader.removeAttribute('hidden');
		}else{
			app.loader.setAttribute('hidden', true);
		}
	}

	app.getDetalhesChamado = function(idChamado){
		var chamado = null;
		var btnRes = app.dialogDetalhe.querySelector('#btnResChamado');
		var inf = app.dialogDetalhe.querySelector('.principal-chamado');
		var res = app.dialogDetalhe.querySelector('.resolucao-chamado');
		for(var i=0;i<app.listaChamados.length;i++){
			chamado = app.listaChamados[i];
			if(chamado.id == idChamado){
				try{
					app.dialogDetalhe.querySelector('.dataAbertura span').textContent = chamado.dataAbertura;
					app.dialogDetalhe.querySelector('.categoria span').textContent = chamado.categoria.descricao;
					app.dialogDetalhe.querySelector('.descricao span').textContent = chamado.descricao;
					app.dialogDetalhe.querySelector('.endereco span').textContent = 
						chamado.endereco.logradouro + " " + chamado.endereco.numero;
					app.dialogDetalhe.querySelector('.tecnico span').textContent = chamado.tecnico.nome;
					app.dialogDetalhe.querySelector('.dataFechamento span').textContent = chamado.dataFechamento;
					app.dialogDetalhe.querySelector('.resolucao span').textContent = chamado.resolucao;

					if(chamado.dataFechamento){
						btnRes.removeAttribute('hidden');
					}else{
						btnRes.setAttribute('hidden', true);
						res.setAttribute('hidden', true);
						inf.removeAttribute('hidden');
					}
					break;
				}catch(ex){
					console.log(ex);
				}
			}
		}

	}

	app.registrarChamado = function(){
	    var selectCateg = document.getElementById('selectCategoria');
	    var categSelected = selectCateg.options[selectCateg.selectedIndex];
	    var idCateg = categSelected.value;
	    var txtCateg = categSelected.textContent;
	    var descricao = document.getElementById('descricao').value;
	    var cep = document.getElementById('cep').value;
	    var endereco = document.getElementById('endereco').value;
	    var numero = document.getElementById('numero').value;
	    var date = new Date();
	    var chamado = {
	    	id: date.getMilliseconds(),
	    	dataAbertura: date,
	    	categoria: {
	    		id:idCateg, descricao:txtCateg
	    	},
	    	descricao: descricao,
	    	endereco:{
	    		logradouro:endereco,
	    		numero:numero,
	    		cep:cep
	    	},
	    	status:{id:1, descricao:"ABERTO"}

	    }
	    app.listaChamados.push(chamado);
	}


	app.atualizarListaUsuario = function(idUsuario){
		//mock
		if(app.listaChamados.length == 0){
			app.listaChamados = [
				{
					id: 120,
					dataAbertura: "2018-12-31 16:03:00",
					ipAbertura: "132.122.122.23",
					usuarioAbertura: 
						{
							id:1001,
							nome: "Josias Manoel",
							cpf: "22232123490",
							endereco:{
								logradouro: "Avenida B",
								numero: "27A",
								cep: "02833130",
								bairro: "Vl Flávia",
								cidade: "S Mateus"
							},
							email:"haha@teste.com"
						},
					endereco:{
						logradouro: "Avenida B",
						numero: "27A",
						cep: "02833130",
						bairro: "Vl Flávia",
						cidade: "S Mateus"
					},
					categoria:{id:"1", descricao: "Hardware"},
					status:{id:"1", descricao: "ABERTO"},
					descricao:"Meu monitor não liga",
					tecnico:{id:9, nome:"HECIO" }
				},
				{
					id: 220,
					dataAbertura: "2018-12-31 19:15:00",
					dataFechamento: "2019-01-02 09:02:00",
					ipAbertura: "144.109.35.41",
					usuarioAbertura: 
						{
							id:1999,
							nome: "Rob Marcondes",
							cpf: "18541002870",
							endereco:{
								logradouro: "Rua Frescanilza da Silva Neto Carvalho de Souza",
								numero: "101",
								complemento: "Bloco 25 Casa 170",
								cep: "05002090",
								bairro: "Jardins Verdes",
								cidade: "Sem favelas"
							},
							email:"kkk@teste.com"
						},
					endereco:{
						logradouro: "Rua Frescanilda da Silva Neto Carvalho de Souza",
						numero: "103",
						cep: "05002090",
						bairro: "Jardins Verdes",
						cidade: "Sem Favelas",
						complemento: "Casa de mamãe"
					},
					categoria:{id:"1", descricao: "Hardware"},
					status:{id:"2", descricao: "RESOLVIDO"},
					descricao:"Minha mamãe tem um mouser mais acho que estar quebrado porque ele não funciona, as vezes ele funciona e as vezes ele não quer funcionar.",
					resolucao:"Mouse com o cabo USB danificado.Substituído.",
					tecnico:{id:9, nome:"HECIO" }
				}
			];
		}
		app.mostrarListaChamados();
		/*
	    var statement = 'select * from weather.forecast where woeid=' + key;
	    var url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss?format=json&q=' +
	        statement;
	    // TODO add cache logic here

	    // Fetch the latest data.
	    var request = new XMLHttpRequest();
	    request.onreadystatechange = function() {
	      if (request.readyState === XMLHttpRequest.DONE) {
	        if (request.status === 200) {
	          var response = JSON.parse(request.response);
	          var results = response.query.results;
	          results.key = key;
	          results.label = label;
	          results.created = response.query.created;
	          app.updateForecastCard(results);
	        }
	      } else {
	        // Return the initial weather forecast since no data is available.
	        app.updateForecastCard(initialWeatherForecast);
	      }
	    };
	    request.open('GET', url);
	    request.send();
	    */
	}


document.getElementById('btnAtualizar').addEventListener('click', function() {
	app.atualizarChamados();
});

document.getElementById('btnAbreChamado').addEventListener('click', function() {
	app.toggleDlgAbreChamado(true);
});

document.getElementById('btnCancelaAbreChamado').addEventListener('click', function() {
	app.toggleDlgAbreChamado(false);
});

document.getElementById('btnCancelaDetChamado').addEventListener('click', function() {
	app.toggleDlgDetalhesChamado(false);
});

document.getElementById('btnInfChamado').addEventListener('click', function() {
	app.toggleInfChamado(true);
});

document.getElementById('btnResChamado').addEventListener('click', function() {
	app.toggleInfChamado(false);
});

document.getElementById('btnConfirma').addEventListener('click', function() {
	app.registrarChamado();
});


document.getElementById('btnProxima').addEventListener('click', function() {
	var etapa = app.getSelEtapaChamado(this);
	if(etapa > 0){
		app.changeEtapaChamado(etapa+1);	
	}
});

document.getElementById('btnAnterior').addEventListener('click', function() {
	var etapa = app.getSelEtapaChamado(this);
	if(etapa > 0){
		app.changeEtapaChamado(etapa-1);	
	}
});

